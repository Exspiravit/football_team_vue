export default [
    {
        name:'Pedro Gallese',
        position:'Arquero',
        goals:0
    },
    {
        name:'Carlos Cáceda',
        position:'Arquero',
        goals:0
    },
    {
        name:'José Carvallo',
        position:'Arquero',
        goals:0
    },
    {
        name:'Luis Advíncula',
        position:'Defensor',
        goals:0
    },
    {
        name:'Aldo Corzo',
        position:'Defensor',
        goals:0
    },
    {
        name:'Christian Ramos',
        position:'Defensor',
        goals:0
    },
    {
        name:'Alberto Rodríguez',
        position:'Defensor',
        goals:0
    },
    {
        name:'Miguel Araujo',
        position:'Defensor',
        goals:0
    },
    {
        name:'Anderson Santamaría',
        position:'Defensor',
        goals:0
    },
    {
        name:'Miguel Trauco',
        position:'Defensor',
        goals:0
    },
    {
        name:'Nilson Loyola',
        position:'Defensor',
        goals:0
    },
    {
        name:'Pedro Aquino',
        position:'Mediocampista',
        goals:0
    },
    {
        name:'Wilder Cartagena',
        position:'Mediocampista',
        goals:0
    },
    {
        name:'Renato Tapia',
        position:'Mediocampista',
        goals:0
    },
    {
        name:'Yoshimar Yotún',
        position:'Mediocampista',
        goals:0
    },
    {
        name:'Edison Flores',
        position:'Mediocampista',
        goals:0
    },
    {
        name:'Paolo Hurtado',
        position:'Mediocampista',
        goals:0
    },
    {
        name:'Christian Cueva',
        position:'Mediocampista',
        goals:0
    },
    {
        name:'André Carrillo',
        position:'Delantero',
        goals:0
    },
    {
        name:'Andy Polo',
        position:'Delantero',
        goals:0
    },
    {
        name:'Raúl Ruidíaz',
        position:'Delantero',
        goals:0
    },
    {
        name:'Jefferson Farfán',
        position:'Delantero',
        goals:0
    },
    {
        name:'Paolo Guerrero',
        position:'Delantero',
        goals:0
    },
    
]